// import 'react-native';
// import React from 'react';
// import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
// import { shallow, configure } from 'enzyme';
// // import renderer from 'react-test-renderer';
// import { Provider } from 'react-redux';
// import configureStore from 'redux-mock-store';
// import Login from '../src/container/pages/Login';

// configure({ adapter: new Adapter(), disableLifecycleMethods: true });
// const initialState = { isLoading: false };
// const mockStore = configureStore();
// const store = mockStore(initialState);
// const profileWrapper = shallow(<Provider store={store}><Login /></Provider>);

// describe('Login page', () => {
//   describe('Button login', () => {
//     it('Event test', () => {
//       const mockCallback = jest.fn();
//       profileWrapper.render();
//       expect(mockCallback.mock.calls.length);
//     });
//   });
// });

// import 'react-native';
// import React from 'react';
// import renderer from 'react-test-renderer';
// import { Provider } from 'react-redux';
// import configureStore from 'redux-mock-store';
// import Login from '../src/container/pages/Login';

// test('Login snapshot', () => {
//   const initialState = { isLoading: false };
//   const mockStore = configureStore();
//   const store = mockStore(initialState);
//   const snap = renderer.create(<Provider store={store}><Login /></Provider>).toJSON();
//   expect(snap).toMatchSnapshot();
// });

import reducer, { initialState } from '../src/redux/reducers/global';

test('Should return initial state', () => {
  expect(
    reducer(undefined, {
      type: undefined,
    }),
  ).toEqual(initialState);
});
