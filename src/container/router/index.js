import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';
import NetInfo from '@react-native-community/netinfo';
import {
  Splash,
  Login,
  Register,
  SuccessRegister,
  Home,
  Detail,
  NoInternet,
} from '../pages';

const Stack = createStackNavigator();

function Router() {
  const isLogin = useSelector((state) => state.app.isLogin);

  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);

  function checkOfflineLogin() {
    if (isOffline) {
      return (
        <Stack.Screen
          name="MainApp"
          component={NoInternet}
          options={{ headerShown: false }}
        />
      );
    } if (isLogin) {
      return (
        <>
          <Stack.Screen
            name="MainApp"
            component={Home}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Detail"
            component={Detail}
            options={{ headerShown: false }}
          />
        </>
      );
    }
    return (
      <>
        <Stack.Screen
          name="MainApp"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SuccessRegister"
          component={SuccessRegister}
          options={{ headerShown: false }}
        />
      </>
    );
  }

  return (
    <Stack.Navigator initialRouteName="Splash">
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{ headerShown: false }}
      />
      {checkOfflineLogin()}
    </Stack.Navigator>
  );
}

export default Router;
