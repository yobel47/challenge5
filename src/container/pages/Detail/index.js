import {
  Text,
  View,
  Dimensions,
  BackHandler,
  StatusBar,
  Share,
  FlatList,
  Alert,
} from 'react-native';
import { Button } from 'react-native-paper';
import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import { formatCurrency } from 'react-native-format-currency';
import LottieView from 'lottie-react-native';
import { primaryColor } from '../../utils/colors';
import { Poster, Title } from '../../components';
import styles from './styles';
import { getDataDetail, setLoading } from '../../../redux/actions';
import { LoadingImage } from '../../assets';
import {
  configure,
  createChannel,
  sendNotification,
} from '../../../notification';

const window = Dimensions.get('screen');

function Detail({ route, navigation }) {
  const { id } = route.params;

  const isLoading = useSelector((state) => state.app.isLoading);
  const userData = useSelector((state) => state.login.userData);
  const detailBookData = useSelector((state) => state.detailData.bookDetailData);

  const dispatch = useDispatch();
  const getDetailBookData = (bookId, token) => dispatch(getDataDetail(bookId, token));
  const actionSetLoading = (loading) => dispatch(setLoading(loading));

  useEffect(() => {
    getDetailBookData(id, userData.tokens.access.token);
    const backAction = () => {
      actionSetLoading(true);
      navigation.navigate('MainApp');
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  const getPrice = (price) => {
    const [valueFormattedWithSymbol] = formatCurrency({ amount: price, code: 'IDR' });
    return valueFormattedWithSymbol;
  };

  const onShare = async (shareTitle) => {
    try {
      await Share.share({
        message: shareTitle,
      });
    } catch (error) {
      Alert.alert(error);
    }
  };

  const actionLove = (likedTitle) => {
    configure();
    createChannel(id);
    sendNotification(id, 'Book', `You liked ${likedTitle}`);
  };

  return (
    <View>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LottieView
            source={LoadingImage}
            style={styles.loadingImage}
            autoPlay
            loop
          />
        </View>
      ) : (
        <FlatList
          onRefresh={() => getDetailBookData(id, userData.tokens.access.token)}
          refreshing={isLoading}
          data={detailBookData}
          keyExtractor={(item) => item.id}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <View>
              <View style={styles.headerContainer}>
                <View style={styles.headerImage}>
                  <Poster
                    imgUrl={item.cover_image}
                    imgSize={{
                      width: window.width * 0.36,
                      height: window.width * 0.55,
                    }}
                  />
                </View>
                <View style={styles.headerTextContainer}>
                  <Text style={styles.headerTextTitle}>{item.title}</Text>
                  <Text style={styles.headerTextBy}>Written by:</Text>
                  <Text style={styles.headerText}>{item.author}</Text>
                  <Text style={styles.headerTextBy}>Published by:</Text>
                  <Text style={styles.headerText}>{item.publisher}</Text>
                </View>
              </View>
              <View style={styles.infoContainer}>
                <View style={styles.infoContentContainer}>
                  <Text style={styles.infoTextTitle}>Rating</Text>
                  <Text style={[styles.infoText]}>
                    {item.average_rating}
                    {' '}
                    <Icon name="star" size={24} />
                  </Text>
                </View>
                <View style={styles.infoContentContainer}>
                  <Text style={styles.infoTextTitle}>Total Sale</Text>
                  <Text style={[styles.infoText]}>{item.total_sale}</Text>
                </View>
                <View style={styles.buyButton}>
                  <Button mode="contained" onPress={() => {}}>
                    Buy
                    {' '}
                    {getPrice(item.price)}
                  </Button>
                </View>
              </View>
              <View style={styles.overviewContainer}>
                <Title text="Overview" />
                <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 18 }}>
                  {item.synopsis}
                </Text>
              </View>
              <View style={styles.backButton}>
                <Button
                  icon="arrow-left-thick"
                  mode="contained"
                  style={{
                    marginLeft: 16,
                  }}
                  onPress={() => {
                    navigation.navigate('MainApp');
                    actionSetLoading(true);
                  }}
                />
              </View>
              <View style={styles.rightButtonContainer}>
                <View style={styles.heartButton}>
                  <Button
                    icon="heart-outline"
                    mode="contained"
                    style={{
                      marginLeft: 16,
                    }}
                    onPress={() => actionLove(item.title)}
                  />
                </View>
                <View style={styles.shareButton}>
                  <Button
                    icon="share"
                    mode="contained"
                    style={{
                      marginLeft: 16,
                    }}
                    onPress={() => onShare(item.title)}
                  />
                </View>
              </View>
            </View>
          )}
        />
      )}
    </View>
  );
}

export default Detail;
