import { SET_LOADING, SET_ALERT, SET_LOGIN } from '../types';

export const initialState = {
  alertMessage: '',
  isLoading: false,
  isLogin: false,
  showAlert: false,
};

const GlobalReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_ALERT:
      return {
        ...state,
        showAlert: action.isVisible,
        alertMessage: action.message,
      };
    case SET_LOGIN:
      return {
        ...state,
        isLogin: action.isLogin,
      };
    default:
      return state;
  }
};

export default GlobalReducer;
