import { combineReducers } from 'redux';
import GlobalReducer from './global';
import LoginReducer from './login';
import RegisterReducer from './register';
import GetData from './getData';
import GetDetailData from './getDetailData';

const AllReducers = combineReducers(
  {
    app: GlobalReducer,
    login: LoginReducer,
    register: RegisterReducer,
    data: GetData,
    detailData: GetDetailData,
  },
);

export default AllReducers;
