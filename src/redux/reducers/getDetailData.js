import { GET_DETAIL_SUCCESS, GET_DETAIL_FAILED } from '../types';

const initialState = {
  bookDetailData: [],
};

const DetailDataReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case GET_DETAIL_SUCCESS:
      return {
        ...state,
        bookDetailData: [action.payload],
        isLoading: false,
      };
    case GET_DETAIL_FAILED:
      return {
        ...state,
        errorMessage: action.message,
        isLogin: false,
        isLoading: false,
        showAlert: true,
      };
    default:
      return state;
  }
};

export default DetailDataReducer;
